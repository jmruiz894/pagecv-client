
import './App.css';
import Header from './screens/header/header'
import AboutMe from './screens/aboutme/aboutme'
import Experience from './screens/experience/experience';
import Education from './screens/education/education'
import Skills from './screens/skills/skills';
import Contact from './screens/contact/contact';

function App() {
    return (
        <div className="App">
            <Header/>
            <AboutMe/>
            <Experience/>
            <Education/>
            <Skills/>
            <Contact/>
        </div>
    );
}

export default App;
