import React from 'react'
import ListSkills from '../../components/lists/listSkills';

const Skills = () => {

    const icons = [
        {name: "HMTL5", fab: "fab fa-html5"},
        {name: "CSS3", fab: "fab fa-css3-alt"},
        {name: "Javascript", fab: "fab fa-js-square"},
        {name: "ReactJS", fab: "fab fa-react"},
        {name: "NodeJS", fab: "fab fa-node-js"},
        {name: "NPM", fab: "fab fa-npm"},
        {name: "Yarn", fab: "fab fa-yarn"},
        {name: "Bootstrap", fab: "fab fa-bootstrap"},
        {name: "Sass", fab: "fab fa-sass"},
        {name: "Git", fab: "fab fa-git-square"},
        {name: "Sourcetree", fab: "fab fa-sourcetree"},
        {name: "Github", fab: "fab fa-github"},
        {name: "Bitbucket", fab: "fab fa-bitbucket"},
        {name: "Amazon Web S.", fab: "fab fa-aws"},
        {name: "MySQL", fab: "fas fa-database"}
    ]

    return (
        <section className="skills-section" id="skills">
            <h1 className="mb-5">Habilidades</h1>
            <div className="container">
                <h2 className="mb-5 text-center text-white"> Lenguajes de Programación y Herramientas </h2>
                <ul className="list-inline dev-icons">
                    {
                        icons.map((ic) => (
                            <ListSkills ic={ic}/>
                        ))
                    }
                </ul>
                <hr className="d-none d-lg-block mt-5 mb-0"/>
                <h2 className="mt-5 mb-5 text-center text-white"> Flujo de Trabajo </h2>
                <ul>
                    <li>
                        <h5 className="mb-1">
                           Implementacion de Metodologías Scrum para desarrollo.
                        </h5>
                    </li>
                    <li>
                        <h5 className="mb-1">
                            Diseño basado en "Mobile first", en pro de un desarrollo "Responsive".
                        </h5>
                    </li>
                    <li>
                        <h5 className="mb-1">
                            Adaptacion en diferentes entornos de trabajo.
                        </h5>
                    </li>
                </ul>

            </div>
        </section>
    );
}

export default Skills