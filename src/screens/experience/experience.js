import React from 'react'
import PrimaryCard from '../../components/card/recentCard/primaryCard';
import SecondaryCard from '../../components/card/timelineCard/timelineCard';

const Experience = () => {

    return (
        <section class="experience-section bg-light" id="experience">
            <h1 className="mb-5">Experiencia</h1>
            <div className="container">
                <PrimaryCard />
                <SecondaryCard />
            </div>
        </section>
    );
}

export default Experience