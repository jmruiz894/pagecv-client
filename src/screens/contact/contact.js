import React from 'react'
import ContactCard from '../../components/card/contactCard/contactCard'
import FormContact from '../../components/form/formContac'

const Contact = () => {

    const iconStyle = "text-primary mb-2";
    const dataCont = [
        { favi: `fas fa-map-marked-alt ${iconStyle}`, data: "Direción", res: "CABA, Argentina" },
        { favi: `fas fa-envelope ${iconStyle}`, data: "Email", res: "jmruiz0894@gmail.com" },
        { favi: `fas fa-mobile-alt ${iconStyle}`, data: "Teléfono", res: "+54 (011) 23897146" }
    ]
    return (
        <section className="contact-section bg-white" id="contact">
            <div className="container">
                <div className="row">
                    <FormContact/>
                </div>
                <div className="row mt-5">
                    {dataCont.map((dc) => (
                            <ContactCard dc={dc}/>
                        ))}
                </div>
            </div>
        </section>
    )
}

export default Contact
