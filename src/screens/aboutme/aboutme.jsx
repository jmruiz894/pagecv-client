import React from 'react'

const AboutMe = () => {
    return (
        <section className="about-section text-center" id="about-me">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-lg-8 mx-auto">
                        <h1 className="text-white mb-5">Sobre Mi</h1>
                        <h5 className="text-white">
                        En el ámbito laboral, me destaco por ser una persona que dedica mucha confianza en mi capacidad de adaptación ante los retos laborales en conjunto con equipos de trabajo variados y proactivos. 
                        He adquirido valiosos conocimientos tecnológicos y mantengo el interés en sacar provecho de la formación de nuevas habilidades en mi carrera profesional, considero de gran importancia un clima laboral óptimo donde pueda, con iniciativa y responsabilidad, afrontar todo tipo de retos.
                        </h5>
                        <hr className="d-none d-lg-block mt-5 mb-0"/>
                    </div>
                </div>
                <div className="social d-flex justify-content-center mb-5">
                    <a className="mx-2" href="https://www.linkedin.com/in/jose-miguel-ruiz-ramirez/" target="_blank" rel="noopener noreferrer"><i className="fab fa-linkedin-in"></i></a>
                    <a className="mx-2" href="https://www.facebook.com/josee.ruiiz.5/" target="_blank" rel="noopener noreferrer"><i className="fab fa-facebook-f"></i></a>
                    <a className="mx-2" href="https://bitbucket.org/jmruiz894/" target="_blank" rel="noopener noreferrer"><i className="fab fa-bitbucket"></i></a>
                </div>
            </div>
        </section>
    );
}

export default AboutMe