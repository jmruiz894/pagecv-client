import React from 'react';
import img from './educacion.jpg'

const Education = () => {
    return (
        <section className="education-section text-center bg-light" id="education">
            <h1 className="mb-5">Educación</h1>
            <div className="container">
                <div className="row justify-content-center no-gutters">
                    <div className="col-lg-6"><img className="img-fluid" src={img} alt="..." /></div>
                    <div className="col-lg-6 order-lg-first">
                        <div className="bg-secondary text-center h-100">
                            <div className="d-flex h-100">
                                <div className="educ-text w-100 my-auto text-center text-lg-right">
                                    <h4 className="text-white">
                                        U.E. Colegio Brito, Venezuela
                                    </h4>
                                    <p className="mb-0 text-white">
                                        Bachiller en Ciencias 2009−2011
                                    </p>
                                    <hr className="d-none d-lg-block mb-0 mr-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Education;