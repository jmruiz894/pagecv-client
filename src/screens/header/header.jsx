import React from 'react'
import Navbar from '../../components/navbar/navbar'

const Header = () => {

    return (
        <header className="masthead" id="page-top">
            <Navbar/>
            <div className="container d-flex h-100 align-items-center">
                <div className="subheading mx-auto text-center">
                    <h1 className="mx-auto my-0 text-uppercase">José M. Ruiz</h1>
                    <a className="btn btn-primary js-scroll-trigger" href="#about-me"> Comencemos </a>
                </div>
            </div>
        </header>
    )
};

export default Header