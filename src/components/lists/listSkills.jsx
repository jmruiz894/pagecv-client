import React from 'react'

const ListSkills = ({ ic }) => {
    return (
        <>
            {
                <li className="list-inline-item">
                    <i className={ic.fab}></i>
                    <h5>{ic.name}</h5>
                </li>
            }
        </>
    );
}

export default ListSkills;