import React, { useState} from 'react'

const FormContact = () => {
    const [email, setEmail] = useState('');
    const [nombre, setNombre] = useState('');

    const wap = `https://api.whatsapp.com/send?phone=5491123897146&text=Hola%20Jose%20Miguel,%20mi%20nombre%20es%20 + ${nombre} + ,%20me%20interesa%20tu%20perfil%20y%20me%20gustaria%20acordar%20un%20entrevista%20contigo,%20este%20es%20mi%20correo + ${email} + %20y%20mi%20numero%20de%20contacto`;

    const resetForm = () => {
        document.getElementById("formContact").reset();
    }
    const sendMessage = (e) => {
        e.preventDefault();
        window.open(wap,'_blank');
        resetForm();
    };

    return (
        <div className="col-md-12 col-lg-9 mx-auto text-center">
            <i className="far fa-paper-plane fa-2x mb-2 text-white"/>
            <h2 className="text-white">
                ¡Estoy abierto a nuevos retos y propuestas laborales!
            </h2>
            <h2 className="text-white mb-5">
                Si viendo mi perfil crees que podemos trabajar en equipo, acordemos una entrevista, deja tu email acá..
            </h2>
            <form className="form-inline d-flex" onSubmit={sendMessage} id="formContact">
                <input 
                    className="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0"
                    type="text" 
                    placeholder="Introduzce tu nombre..." 
                    onChange={(e) => {setNombre(e.target.value)}} 
                />
                <input 
                    className="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0"
                    type="email" 
                    placeholder="Introduzce tu email..." 
                    onChange={(e) => {setEmail(e.target.value)}} 
                />
                <button 
                    className="btn btn-primary mx-auto" 
                    type="submit"
                >
                    Enviar
                </button>
            </form>
        </div>
    )
}

export default FormContact;
