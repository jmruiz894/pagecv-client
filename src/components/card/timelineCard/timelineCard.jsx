import React from "react";
import JobsListing from './jobsListing';

const TimelineCard = () => {

    const jobs = [
        {
            tie: "Feb 2020 − Oct 2021",
            position: "Desarrollador Web Front-End", 
            employer: "(Freelance)", 
            description: "Trabajo remoto part-time. Desarrollo de mejoras y funcionalidades nuevas a nivel de cliente rest api basados en React JS. Entre las que destacan, la api de métricas y panel de visualización en el sistema admin, mejoras al flujo de compra, listado de artículos y selectores de color y talle, en la vista de artículos."
        },
        {
            tie: "Sep 2014 − Jul 2017",
            position: "Analista de Atención al Cliente", 
            employer: "(Clínica Prevaler, Venezuela.)", 
            description: "Documentación y organización de datos de pacientes. Estudiar e interpretar las características y necesidades del paciente para proceder a la solicitud de aprobación por parte de la institución pertinente la aprobación de consultas o estudios."
        }
    ]

    return (
        <div className="container">
            {
                jobs.map((jl) => (
                    <JobsListing jl={jl} />
                ))
            }
        </div>
    );
};

export default TimelineCard;
