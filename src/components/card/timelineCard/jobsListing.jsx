/* eslint-disable jsx-a11y/heading-has-content */
import React from 'react'

const JobsListing = ({ jl }) => {
    return (
        <>
            {
                <div className="timeline-item" date-is={jl.tie}>
                    <h2>
                        {jl.position}{" "}{jl.employer}
                    </h2>
                    <p>
                        {jl.description}
                    </p>
                </div>
            }
        </>
    );
}

export default JobsListing