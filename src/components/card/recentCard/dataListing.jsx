import React from 'react';

const DataListing = ({ dl }) => {
    return (
        <>
            {
                <li>
                    <p className="text-black-50 mb-0">
                        {dl.dl}
                    </p>
                </li>
            }
        </>
    );
}

export default DataListing;