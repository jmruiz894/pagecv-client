import React from "react";
import imagen from "../assets/img/alkemy.jpeg";
import DataListing from "./dataListing";

const PrimaryCard = () => {

    const dataA = [
        { dl: "Creación de modelos y migraciones para la manipulación de datos." },
        { dl: "Aplicación de testing a las diferentes rutas del servicio." },
        { dl: "Definición de rutas y Servicios para el CRUD de los modelos Categorias y Novedades." }
    ]
    const dataB = [
        { dl: "Apartados de backoffice para el listado, adición, edición y borrado de elementos." },
        { dl: "Estructura responsiva a distintas páginas del sitio." },
        { dl: "Formulario de edición de Usuarios." },
        { dl: "Desarrollo del Pie de página del sitio web." }
    ]

    return (
        <div className="row align-items-center no-gutters mb-4 mb-lg-5 pb-5">
            <div className="col-xl-7 col-lg-6">
                <img
                className="img-fluid mb-3 mb-lg-0"
                src={imagen}
                alt="..."
                />
            </div>
            <div className="col-xl-5 col-lg-6">
                <div className="featured-text text-center text-lg-left">
                    <div className="d-flex flex-column flex-md-row justify-content-between">
                        <div className="flex-grow-1">
                            <h2>Desarrollador Web Fullstack</h2>
                            <h4>(Alkemy)</h4>
                        </div>
                        <div className="flex-shrink-0"><span className="text-primary">Feb 2021 - May 2021</span></div>
                    </div>
                    <p className="text-black mb-2">
                        Proyecto: Servicio web ONG Manos de la Cava.
                    </p>
                    <p className="text-black mb-2">
                        API Rest Service para acceso a datos, CRUD y publicacion de recursos.
                    </p>
                    <ul>
                        {dataA.map((dl) => (
                            <DataListing dl={dl}/>
                        ))}
                    </ul>
                    <p className="text-black-50 mb-2">
                        Tecnologias: Express, Sequelize, MySQL, Testing (Mocha, Chai), Express Validator, S3 de AWS.
                    </p>
                    <p className="text-black mb-2">
                        Cliente de Aplicación Web
                    </p>
                    <ul>
                        {dataB.map((dl) => (
                            <DataListing dl={dl}/>
                        ))}
                    </ul>
                    <p className="text-black-50 mb-2">
                        Tecnologias: ReactJS, Material UI, Formik Forms, React Redux, SweetAlert.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default PrimaryCard;
