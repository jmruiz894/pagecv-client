import React from 'react'

const ContactCard = ({ dc }) => {
    return (
        <>    
            {
                <div className="col-md-4 mb-3 mb-md-0">
                    <div className="card py-4 h-100">
                        <div className="card-body text-center">
                            <i className={dc.favi}/>
                            <h4 className="text-uppercase m-0 text-white-50">
                                {dc.data}
                            </h4>
                            <hr className="my-4" />
                            <div className="small text-white">
                                {dc.res}
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default ContactCard;
