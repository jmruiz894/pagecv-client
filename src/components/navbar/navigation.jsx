import React from 'react'

const Navigation = ({ m }) => {
    return (
        <>
            {
                <li className="nav-item" key={m.name}>
                    <a className="nav-link js-scroll-trigger" exact href={m.href}>
                        {" "}
                        {m.name}
                        {" "}
                    </a>
                </li>
            }
        </>
    );
}

export default Navigation
