import React from 'react'
import Navigation from "./navigation"

const Navbar = () => {

    const menu = [
        { href: "#about-me", name: "Sobre mi" },
        { href: "#experience", name: "Experiencia" },
        { href: "#education", name: "Educación" },
        { href: "#skills", name: "Habilidades" },
        { href: "#contact", name: "Contacto" },
    ];


    return (
        <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div className="container">
                <a className="navbar-brand js-scroll-trigger" href="#page-top">CV Online</a>
                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i className="ml-2 fas fa-bars"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                        {menu.map((m) => (
                            <Navigation key={m.href} m={m} />
                        ))}
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Navbar